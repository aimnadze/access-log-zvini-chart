#!/usr/bin/php
<?php

chdir(__DIR__);
include_once 'config.php';

include_once 'fns/call_zvini_method.php';
call_zvini_method('doNothing', [], $zvini_location, $api_key);

include_once 'fns/get_processed_files.php';
$processed_files = get_processed_files();

include_once 'fns/clean_processed_files.php';
clean_processed_files($processed_files);

foreach ($charts as &$chart) $chart['bars'] = [];
unset($chart);

foreach ($globs as $glob) {

    if ($verbose) echo "INFO: Processing glob $glob\n";

    $files = glob($glob);
    foreach ($files as $file) {

        if ($verbose) echo "INFO: Processing file $file\n";

        $realpath = realpath($file);
        if (array_key_exists($realpath, $processed_files)) continue;
        else $processed_files[$realpath] = true;

        $lineNumber = 0;
        $f = fopen($file, 'r');
        while (true) {

            $lineNumber++;

            $line = fgets($f);
            if ($line === false) break;
            $line = json_decode($line);

            if (!is_object($line) || !property_exists($line, 'request')) {
                echo "WARN: line $lineNumber is invalid in file $file\n";
                continue;
            }

            $requestTime = $line->request->time;
            $monthly_key = substr($requestTime, 0, 7);
            $daily_key = substr($requestTime, 0, 10);

            foreach ($charts as &$chart) {

                if (array_key_exists('host', $chart)) {
                    $headers = $line->request->headers;
                    if (!property_exists($headers, 'host') ||
                        $headers->host !== $chart['host']) continue;
                }

                if ($chart['interval'] == 'daily') $key = $daily_key;
                else $key = $monthly_key;

                $bars = &$chart['bars'];
                if (!array_key_exists($key, $bars)) $bars[$key] = 0;
                $bars[$key]++;
                unset($bars);

            }
            unset($chart);


        }
        fclose($f);

    }
}

include_once 'fns/put_bars.php';
foreach ($charts as $chart) {
    $bars = $chart['bars'];
    if (!$bars) {
        if ($verbose) echo "INFO: Nothing to do for bar chart #$chart[id]\n";
        continue;
    }
    if ($verbose) echo "INFO: Updating bar chart #$chart[id]\n";
    put_bars($chart['id'], $chart['bars'], $zvini_location, $api_key);
}

include_once 'fns/set_processed_files.php';
set_processed_files($processed_files);
