Access Log Zvini Chart
======================

The program reads logs files of either
[Reverse Proxy Server](https://gitlab.com/aimnadze/reverse-proxy-server) or
[Forward Proxy Server](https://gitlab.com/aimnadze/forward-proxy-server),
aggregates the number of requests and puts the data in
a [Zvini](https://gitlab.com/zvini/website) instance. Its features include:
* Read files from multiple locations.
* Aggregate data daily or monthly.
* Aggregate data based on HTTP host header.

See Also
--------

* [Zvini Website](https://gitlab.com/zvini/website)
* [Reverse Proxy Server](https://gitlab.com/aimnadze/reverse-proxy-server)
* [Forward Proxy Server](https://gitlab.com/aimnadze/forward-proxy-server)
