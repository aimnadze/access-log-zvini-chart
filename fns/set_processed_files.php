<?php

function set_processed_files ($processed_files) {
    $content =
        "<?php\n\n"
        ."function get_processed_files () {\n"
        .'    return '.var_export($processed_files, true).";\n"
        ."}\n";
    file_put_contents(__DIR__.'/get_processed_files.php', $content);
}
