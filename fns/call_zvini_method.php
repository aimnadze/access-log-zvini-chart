<?php

function call_zvini_method ($method, $params, $zvini_location, $api_key) {

    $params['api_key'] = $api_key;

    $ch = curl_init();
    curl_setopt_array($ch, [
        CURLOPT_URL => "{$zvini_location}api-call/$method",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POSTFIELDS => $params,
    ]);
    $response = curl_exec($ch);

    if ($response === false) {
        echo 'ERROR: '.curl_error($ch)."\n";
        exit(1);
    }

    if (curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200) {
        echo "ERROR: $response\n";
        exit(1);
    }

    return json_decode($response);

}
