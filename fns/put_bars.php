<?php

function put_bars ($id, $bars, $zvini_location, $api_key) {

    include_once __DIR__.'/call_zvini_method.php';
    $callZviniMethod = function ($method, $params) use (
        $zvini_location, $api_key) {

        return call_zvini_method($method, $params, $zvini_location, $api_key);

    };

    $existingBars = $callZviniMethod('barChart/bar/list', ['id' => $id]);

    foreach ($bars as $label => $value) {
        foreach ($existingBars as $existingBar) {
            if ($existingBar->label === $label) {
                $callZviniMethod('barChart/bar/edit', [
                    'id' => $existingBar->id,
                    'value' => $existingBar->value + $value,
                    'label' => $existingBar->label,
                ]);
                unset($bars[$label]);
                break;
            }
        }
    }

    foreach ($bars as $label => $value) {
        $callZviniMethod('barChart/bar/add', [
            'id' => $id,
            'value' => $value,
            'label' => $label,
        ]);
    }

}
