<?php

function clean_processed_files (&$processed_files) {
    foreach ($processed_files as $processed_file => $value) {
        if (!is_file($processed_file)) {
            unset($processed_files[$processed_file]);
        }
    }
}
